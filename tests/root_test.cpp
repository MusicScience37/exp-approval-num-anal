#include <ApprovalTests.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fmt/format.h>
#include <num_collect/constants/sqrt.h>

TEST_CASE("sqrt") {
    const auto inputs = std::vector<double>{0.0, 0.5, 1.0, 2.0, 123.456};

    ApprovalTests::CombinationApprovals::verifyAllCombinations(
        "sqrt",
        [](double input) {
            const double my_sqrt = num_collect::constants::sqrt(input);
            const double std_sqrt = std::sqrt(input);
            const int precision = 10;

            return fmt::format(
                "Input: {1:.{0}e}\n"
                "my_sqrt:  {2:.{0}e}\n"
                "std_sqrt: {3:.{0}e}",
                precision, input, my_sqrt, std_sqrt);
        },
        inputs);
}
